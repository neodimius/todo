<div class="container">
    <form class="text-center border border-light p-5"  name="editTask" action="/" method="POST" id="editTask">
        <p class="h4 mb-4">Редагування задачі</p>
        <div class="form-group row">
            <label for="user_name" class="col-sm-3 col-form-label">Ім'я користувача</label>
            <div class="col-sm-9">
                <input type="text" class="form-control" id="user_name" name="editTask[user_name]" value="<?= $task['user_name'] ?>" required  maxlength="30">
            </div>
        </div>
        <div class="form-group row">
            <label for="email" class="col-sm-3 col-form-label">Email:</label>
            <div class="col-sm-9">
                <input type="email" class="form-control" id="email" name="editTask[email]" value="<?= $task['email'] ?>" required  maxlength="30">
            </div>
        </div>
        <div class="form-group row">
            <label for="text" class="col-sm-3 col-form-label">Текст задачі:</label>
            <div class="col-sm-9">
                <textarea class="form-control" id="text" name="editTask[text]" maxlength="200" rows="5" cols="35" required><?= $task['text'] ?></textarea>
            </div>
        </div>

        <div class="form-group row">
            <label for="status" class="col-sm-3 col-form-label">Статус виконання<br>(виконано/не виконано):</label>
            <div class="form-check col-sm-1">
                <input class="form-check-input" type="checkbox" name="editTask[status]" id="status" <?= $task['status'] ? 'checked' : '' ?>>
            </div>
        </div>

        <input type="hidden" name="editTask[id]" value="<?= $task['id'] ?>">

        <button type="submit" class="btn btn-primary" form="editTask">Зберегти</button>
    </form>
</div>
