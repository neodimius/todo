<?php if (isset($success) && $success):?>
    <div class="bg-success test-light">
        <?= $success; ?>
    </div>
<?php endif; ?>

<?php if (isset($error) && $error):?>
    <div class="bg-danger test-light">
        <?= $error; ?>
    </div>
<?php endif; ?>
<main class="container" role="main">
    <div class="d-flex justify-content-between align-items-end">
        <div>
            <h3>Всього задач - <?= $tasks['count']; ?>шт</h3>
        </div>
        <div class="align-center">
            <button type="button" class="btn btn-success" data-toggle="modal" data-target="#addTask">
                Додати задачу
            </button>

            <div class="modal fade" id="addTask" tabindex="-1" role="dialog" aria-labelledby="addTaskLabel" aria-hidden="true">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title" id="addTaskLabel">Додати задачу:</h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div class="modal-body">
                            <form name="newTask" action="/" method="POST" id="newTask">
                                <div class="form-group row">
                                    <label for="user_name" class="col-sm-3 col-form-label">Ім'я користувача</label>
                                    <div class="col-sm-9">
                                        <input type="text" class="form-control" id="user_name" name="newTask[user_name]"  maxlength="30" required>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="email" class="col-sm-3 col-form-label">Email:</label>
                                    <div class="col-sm-9">
                                        <input type="email" class="form-control" id="email" name="newTask[email]" required maxlength="30">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="text" class="col-sm-3 col-form-label">Текст задачі:</label>
                                    <div class="col-sm-9">
                                        <textarea class="form-control" id="text" name="newTask[text]" maxlength="200" rows="5" cols="35" required></textarea>
                                    </div>
                                </div>
                            </form>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                            <button type="submit" class="btn btn-primary" form="newTask">Зберегти</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <?php if(isset($tasks['model'])): ?>
    <div class="table">
        <table class="table table-hover table-light">
            <thead>
            <tr class="bg-dark text-light">
                <th scope="col">#</th>
                <th scope="col">Ім'я користувача
                    <a href="/?<?= http_build_query($links['user_name']); ?>">
                        <i class="fa fa-fw fa-sort"></i>
                    </a>
                </th>
                <th scope="col">Email
                    <a href="/?<?= http_build_query($links['email']); ?>">
                        <i class="fa fa-fw fa-sort"></i>
                    </a>
                </th>
                <th scope="col">Текст задачі</th>
                <th class="text-center align-middle" scope="col">Статус задчі
                    <a href="/?<?= http_build_query($links['status']); ?>">
                        <i class="fa fa-fw fa-sort"></i>
                    </a>
                </th>
                <?php if(isset($authStatus) && $authStatus): ?>
                <th>Редагувати</th>
                <?php endif; ?>
            </tr>
            </thead>
            <tbody>

            <?php $n = ($tasks['page'] - 1) * 3; ?>

            <?php foreach ($tasks['model'] as $model) : ?>

                <tr>
                    <th class="text-center align-middle" scope="row"><?= ++$n; ?></th>
                    <td class="align-middle"><?= $model['user_name']; ?></td>
                    <td class="align-middle"><?= $model['email']; ?></td>
                    <td class="align-middle"><?= $model['text']; ?></td>
                    <td class="d-flex flex-column justify-content-center align-items-center">
                    <?php if($model['status']): ?>
                        <span class="btn bg-success font-weight-bold">виконана</span>
                    <?php else: ?>
                        <span class="btn bg-warning font-weight-bold">не виконана</span>
                    <?php endif; ?>

                    <?php if($model['edited']): ?>
                        <span class="text-danger font-weight-bold">відредагована адміністратором</span>
                    <?php endif; ?>
                    </td>
                    <?php if(isset($authStatus) && $authStatus): ?>
                    <td class="text-center align-middle">
                        <a href="/?edit=<?= $model['id'] ?>"><i class="fa fa-fw fa-edit"></i></a>
                    </td>
                    <?php endif; ?>

                </tr>

            <?php endforeach; ?>
            </tbody>
        </table>
    </div>

    <nav aria-label="...">
        <ul class="pagination">
            <?php for ($i =1; $i <= (ceil($tasks['count'] / 3)); $i++): ?>
                <li class="page-item <?= ($i == $tasks['page']) ? 'active' : '' ?>">
                    <a class="page-link" href="/?<?= http_build_query($tasks['currentNavigation']) . '&page=' . $i; ?>">
                        <?= $i; ?>
                        <?php if($i == $tasks['page']): ?>
                            <span class="sr-only">(current)</span>
                        <?php endif; ?>
                    </a>
                </li>
            <?php endfor ?>
        </ul>
    </nav>
    <?php else: ?>
        <div class="bg-warning text-light">
            No Tasks
        </div>
    <?php endif; ?>

</main>