<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
    <link href="//maxcdn.bootstrapcdn.com/font-awesome/4.1.0/css/font-awesome.min.css" rel="stylesheet">
    <title>Tasker</title>
    <style>
        .footer{
            position: absolute;
            bottom: 0;
            width: 100%;
            height: 60px;
        }
    </style>
</head>
<body>
    <div class="navbar navbar-dark bg-dark">
        <div class="container">
            <a class="navbar-brand font-weight-bold text-danger btn btn-light" href="/">
                Задачник
            </a>

            <?php if (isset($authStatus) && $authStatus): ?>
            <div class="col-sm-4">
                <div class="d-flex justify-content-between align-items-center">
                    <span class="text-danger font-weight-bold">Привіт Адмін!</span>
                    <a href="/?logout" class="btn btn-primary">Вийти</a>
                </div>
            </div>
            <?php else: ?>
            <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#exampleModal">
                Авторизація
            </button>
            <?php endif; ?>
            <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title" id="exampleModalLabel">Авторизація:</h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div class="modal-body">
                            <form name="auth" action="/" method="POST" id="auth">
                                <div class="form-group row">
                                    <label for="login" class="col-sm-2 col-form-label">Логін</label>
                                    <div class="col-sm-10">
                                        <input type="text" class="form-control" id="login" name="auth[login]" maxlength="20">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="password" class="col-sm-2 col-form-label">Пароль</label>
                                    <div class="col-sm-10">
                                        <input type="password" class="form-control" id="password" name="auth[password]" maxlength="20">
                                    </div>
                                </div>
                            </form>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Закрити</button>
                            <button type="submit" class="btn btn-primary" form="auth">Авторизація</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

