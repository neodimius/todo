<?php

class Cleaner
{
    public static function clean($array) {
        $result = array();
        foreach ($array as $key => $value) {
            $result[trim(strip_tags($key))] = htmlentities(trim(strip_tags($value)));
        }
        return $result;
    }
}