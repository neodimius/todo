<?php

class Task
{
    private $fields = array('user_name', 'email', 'status', 'id');
    private $limit = 3;
    private $offset = 0;
    private $order;
    private $sort;
    private $page;
    private $currentNavigation;

    public function findAll($navigation) {
        if ($this->validateQuery($navigation))
            return $this->queryAll();
        return false;
    }

    public function find($id) {
        if (is_numeric($id))
            return $this->queryOne($id);
        return false;
    }

    public function newTask($newTask) {
        if ($task = $this->validateTask($newTask))
            $this->addTask($task);
        return false;
    }

    public function editTask($newTask) {
        if ($task = $this->validateTask($newTask, true))
            $this->updateTask($task);
        return false;
    }

    private function validateTask($task, $edit = false) {
        if (!isset($task['user_name']) || !isset($task['email']) || !isset($task['text'])) {
            return false;
        }
        if (!filter_var($task['email'], FILTER_VALIDATE_EMAIL)) {
            return false;
        }
        if($edit) {
            if(!isset($task['status']))
                $task['status'] = 0;
            else
                $task['status'] = 1;

            if ($task['text'] !== $this->queryOne($task['id'])['text'])
                $task['edited'] = 1;
            else
                $task['edited'] = 0;
        }

        return $task;
    }

    private function validateQuery($navigation) {
        if (!isset($navigation['page'])) {
            $navigation['page'] = 1;
        } else {
            if(!is_numeric($navigation['page']))
                return false;
        }

        if (!isset($navigation['order'])) {
            $navigation['order'] = 'id';
        } else {
            if(!in_array($navigation['order'], $this->fields))
                return false;
        }

        if (!isset($navigation['sort'])) {
            $navigation['sort'] = 'DESC';
        } else {
            if($navigation['sort'] !== 'ASC' && $navigation['sort'] !== 'DESC')
                return false;
        }

        $this->page = $navigation['page'];
        $this->offset = (($navigation['page']-1) * $this->limit);
        $this->sort = $navigation['sort'];
        $this->order = $navigation['order'];

        unset($navigation['page']);
        $this->currentNavigation = $navigation;

        return true;
    }

    private function queryAll() {
        $count = Db::connect()->query("SELECT count(*) FROM `tasks`")->fetchColumn();

        $sql = "SELECT * FROM `tasks` ORDER BY $this->order $this->sort LIMIT :limit OFFSET :offset";
        $stmt = Db::connect()->prepare($sql);
        $stmt->bindParam(':limit', $this->limit, PDO::PARAM_INT);
        $stmt->bindParam(':offset', $this->offset, PDO::PARAM_INT);
        $stmt->execute();
        return array(
            'model' => $stmt->fetchAll(PDO::FETCH_ASSOC),
            'count' => $count,
            'links' => $this->linkBuilder(),
            'page' => $this->page,
            'currentNavigation' => $this->currentNavigation,
        );
    }

    private function queryOne($id) {
        $sql = "SELECT * FROM `tasks` WHERE `id` = ?";
        $stmt = Db::connect()->prepare($sql);
        $stmt->bindValue(1, $id, PDO::PARAM_INT);
        $stmt->execute();
        return $stmt->fetch(PDO::FETCH_ASSOC);
    }

    private function linkBuilder() {
        $links = array();
        foreach ($this->fields as $field) {
            if (!in_array($field, $this->fields))
                continue;
            $links[$field]['order'] = $field;
            $links[$field]['sort'] = ($this->order == $field && $this->sort == 'ASC') ? 'DESC' : 'ASC' ;
        }
        return $links;
    }

    private function addTask($task) {
        $sql = "INSERT INTO `tasks` (`user_name`, `email`, `text`) VALUES (?,?,?)";
        $stmt = Db::connect()->prepare($sql);
        $stmt->bindValue(1, $task['user_name']);
        $stmt->bindValue(2, $task['email']);
        $stmt->bindValue(3, $task['text']);
        $stmt->execute();
    }

    private function updateTask($task) {
        $sql = "UPDATE `tasks` SET `user_name` = ?, `email` = ?, `text` = ?, `status` = ?, `edited` = ? WHERE `id` = ?";
        $stmt = Db::connect()->prepare($sql);
        $stmt->bindValue(1, $task['user_name']);
        $stmt->bindValue(2, $task['email']);
        $stmt->bindValue(3, $task['text']);
        $stmt->bindValue(4, $task['status']);
        $stmt->bindValue(5, $task['edited']);
        $stmt->bindValue(6, $task['id']);
        return $stmt->execute();
    }
}
