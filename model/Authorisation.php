<?php

class Authorisation
{
    private $maxSessionTime = 30; // Max Session time to authorization in the minutes;

    public function authUser($login, $pass){
        $sql = "SELECT * FROM `users` WHERE `login` = ? AND `password` = ?";
        $stmt = Db::connect()->prepare($sql);
        $stmt->bindValue(1, $login, PDO::PARAM_STR);
        $stmt->bindValue(2, md5($pass), PDO::PARAM_STR);
        $stmt ->execute();
        $res = $stmt->fetchAll(PDO::FETCH_ASSOC);

        if ($res){
            $token = md5(random_int(20, 10000));   //Generate token
            $expiration_date = time() + $this -> maxSessionTime * 60;
            $id = $res[0]['id'];

            $sql2 = "UPDATE `users` SET `token`= ?, `expiration_date` = ? WHERE `id` = $id";
            $stmt = Db::connect()->prepare($sql2);
            $stmt->bindValue(1, $token);
            $stmt->bindValue(2, $expiration_date);
            $stmt->execute();
            $_SESSION['token'] = $token;
            $_SESSION['login'] = $res[0]['login'];
            return true;
        }
        else return false;
    }

    public function checkUser($login, $token){
        $sql = "SELECT * FROM `users` WHERE `token` = ? AND `login` = ?";
        $stmt = Db::connect() -> prepare($sql);
        $stmt->bindValue(1, $token);
        $stmt->bindValue(2, $login);
        $stmt->execute();
        $res = $stmt->fetchAll(PDO::FETCH_ASSOC);
        $currentTime =time();
        if ($currentTime < $res[0]['expiration_date'] && $res[0]['expiration_date'] != null)
            return true;
        else return false;
    }

    public static function logout(){
        $sql = "UPDATE `users` SET `token`= ?, `expiration_date` = ? WHERE `token` = ? AND `login` = ?";
        $stmt = Db::connect()->prepare($sql);
        $stmt->bindValue(1, null);
        $stmt->bindValue(2, null);
        $stmt->bindValue(3, $_SESSION['token']);
        $stmt->bindValue(4, $_SESSION['login']);
        $stmt->execute();
        session_destroy();
    }
}