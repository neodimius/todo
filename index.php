<?php
    session_start();
    include "autoload.php";

    if(isset($_POST['auth']) && $_POST['auth']) {
        $auth = Cleaner::clean($_POST['auth']);
        $model = new Authorisation();
        $result = $model->authUser($auth['login'], $auth['password']);
        if($result) {
            $success = "Authorisation is done!";
            $authStatus = true;
        } else
            $error = "Wrong Authorisation";
    } elseif (isset($_SESSION['token']) && $_SESSION['token'] && isset($_SESSION['login']) && $_SESSION['login']) {
        if (isset($_GET['logout'])){
            $success = 'Logout was success';
            Authorisation::logout();
        } else {
            $model = new Authorisation();
            $authStatus = $model->checkUser($_SESSION['login'], $_SESSION['token']) ? true : false;
        }
    }

    if (isset($_POST['newTask']) && $_POST['newTask']){
        $newTask = Cleaner::clean($_POST['newTask']);
        $model = new Task();
        $result = $model->newTask($newTask);
        if (isset($result['error']) && $result['error'])
            $error = $result['error'];
        else
            $success = 'Задачу успішно збережено';
    }

    if (isset($_POST['editTask']) && $_POST['editTask']){
        if (isset($authStatus) && $authStatus) {
            $editTask = Cleaner::clean($_POST['editTask']);
            $model = new Task();
            $result = $model->editTask($editTask);
            if (isset($result['error']) && $result['error'])
                $error = $result['error'];
            else
                $success = 'Задачу успішно збережено';
        } else
            $error = 'Немає доступу';
    }

    if(isset($_GET) && $_GET) {
        $navigations = Cleaner::clean($_GET);
    } else
        $navigations = array();

    include 'view/header.php';

    if(isset($navigations['edit']) && isset($authStatus) && $authStatus) {
        $model = new Task();
        $task = $model->find($navigations['edit']);
        include "view/edit.php";
    } else {
        if(isset($navigations['edit']))
            $error = 'Немає доступу';
        $model = new Task();
        $tasks = $model->findAll($navigations);

        $links = $tasks['links'];

        include "view/main.php";
    }

    include 'view/footer.php';